public class FindPrimes {

	public static void main(String[] args){
		int max = Integer.parseInt(args[0]);

		for(int number = 2; number < max; number++) {
			//let divisor = 2
			int divisor = 2;
			// let isPrime = true
			boolean isPrime = true;
			//while divisor less than number
			while (divisor < number) {
				//if the number is divisible by divisor
				if (number % divisor == 0)
					isPrime = false; //isPrime = false
				//increment divisor
				divisor++;

			}

			// if the number is prime
			if (isPrime)
				System.out.println(number + " ");//print the number
		}
	}
}
