import java.awt.*;

public class circle {
    static final double PI = 3.14;
    int radius;
    point center ;

    public circle(int radius,point center){
        this.radius = radius;
        this.center = center;
    }

    public double area(){
        return PI * radius * radius;
    }
    public double perimeter(){
        return 2 * PI * radius;
    }
    public boolean intersect(circle circle){
        //boolean intersect = false;

        return radius + circle.radius >=
                center.distaanceFromPoint(circle.center);

    }
}

