import java.awt.*;

public class rectangle {
    private point topLeft;
    private int width;
    private int height;


    public rectangle(point topLeft, int width, int height) {
        this.topLeft = topLeft;
        this.width = width;
        this.height = height;
    }

    public int area(){
        return width * height;
    }

    public int perimeter(){
        return 2 * (width+height);
    }
    public point[] corners(){
        point[] corners = new point[4];

        corners[0]=topLeft;
        corners[1]=new point(topLeft.getxCoord()+width,topLeft.getyCoord());
        corners[2]=new point(topLeft.getxCoord(),topLeft.getyCoord()-height);
        corners[3]=new point(topLeft.getxCoord()+width,topLeft.getyCoord()-height);


        return corners;
    }
}
