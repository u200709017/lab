public class point {
    private int xCoord;

    public int getyCoord() {
        return yCoord;
    }

    private int yCoord;

    public int getxCoord() {
        return xCoord;
    }

    public point(int xCoord, int yCoord){
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public double distaanceFromPoint(point point){
        int xDiff = xCoord - point.xCoord;
        int yDiff = yCoord - point.yCoord;

        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);

    }
}
